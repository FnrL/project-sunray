﻿using UnityEngine;

[ExecuteInEditMode]
public class PowerSource : PoweredBase 
{

	#region fields

	private Laser _laser;
	private LaserV3 _laserV3;

	#endregion

	#region Constructor

	/// <summary>
	/// Use this for initialization
	/// </summary>
	private void Start ()
	{
		_laser = GetComponent<Laser>();
		_laserV3 = GetComponent<LaserV3>();
	}

	#endregion
	
	#region Methods

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	private new void Update ()
	{
		Execute();
	}

	public override void Execute()
	{
		_laser = GetComponent<Laser>();
		_laserV3 = GetComponent<LaserV3>();

		if (_laser != null)
		{
			_laser.IsAlive = true;
		}

		if (_laserV3 != null)
		{
			_laserV3.IsPowered = true;
		}
	}

	#endregion

}
