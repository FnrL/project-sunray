﻿using System.Collections.Generic;
using UnityEngine;

public class PoweredLever : PoweredBase 
{

	#region Fields

	private bool _isLeverPulled;

	#endregion

	#region EditorFields

	[SerializeField]
	private List<PoweredBase> _connectedComponents;

	#endregion

	#region Constructor

	/// <summary>
	/// Use this for initialization
	/// </summary>
	private void Start () 
	{
		
	}

	#endregion
	
	#region Methods

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	protected override void Update () 
	{
		IsPowered = _isLeverPulled;
		base.Update();
	}

	private void OnDrawGizmos()
	{
		VisualizeConnections(_connectedComponents);
	}

	public override void Execute()
	{
		foreach (var component in _connectedComponents)
		{
			component.IsPowered = true;
		}
	}

	public void SetLeverState(bool isLeverPulled)
	{
		_isLeverPulled = isLeverPulled;
	}

	#endregion

}
