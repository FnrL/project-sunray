﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LaserReceptor : PoweredBase
{

	#region EditorFields

	[SerializeField]
	private List<PoweredBase> _poweredComponentList;

	#endregion

	#region Constructor

	/// <summary>
	/// Use this for initialization
	/// </summary>
	private void Start () 
	{
		
	}

	#endregion
	
	#region Methods

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	protected override void Update () 
	{
		base.Update();
	}

	private void OnDrawGizmos()
	{
		VisualizeConnections(_poweredComponentList);
	}

	public override void Execute()
	{
		foreach (var comp in _poweredComponentList)
		{
			comp.enabled = true;
			comp.IsPowered = true;
		}
	}

	#endregion
}
