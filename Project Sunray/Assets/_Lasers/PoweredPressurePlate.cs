﻿using System.Collections.Generic;
using UnityEngine;

public class PoweredPressurePlate : PoweredBase 
{

	#region EditorFields

	private bool _isPressureOnPlate;

	#endregion

	#region EditorFields

	[SerializeField]
	private List<PoweredBase> _connectedComponents;

	#endregion

	#region Constructor

	/// <summary>
	/// Use this for initialization
	/// </summary>
	private void Start () 
	{
		
	}

	#endregion
	
	#region Methods

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	protected override void Update () 
	{
		
	}

	private void OnDrawGizmos()
	{
		if (_connectedComponents == null || _connectedComponents.Count == 0)
		{
			return;
		}

		foreach (var comp in _connectedComponents)
		{
			if (comp == null)
				continue;

			float distance = (transform.position - comp.transform.position).magnitude;
			if (distance < 0.5f)
			{
				Gizmos.color = comp.IsPowered && _isPressureOnPlate ? Color.green : Color.red;
				Gizmos.DrawSphere(comp.transform.position, 0.2f);
			}
			else
			{
				Debug.DrawLine(transform.position, comp.transform.position, comp.IsPowered && _isPressureOnPlate ? Color.green : Color.red);
			}
		}
	}

	private void OnTriggerStay(Collider other)
	{
		//JFE_AvatarCollider avatarColl = other.GetComponent<JFE_AvatarCollider>();
		//if (avatarColl != null)
		//{
		//	_isPressureOnPlate = true;
		//	Execute();
		//}
	}

	private void OnTriggerExit(Collider other)
	{
		//JFE_AvatarCollider avatarColl = other.GetComponent<JFE_AvatarCollider>();
		//if (avatarColl != null)
		//{
		//	_isPressureOnPlate = false;
		//}
	}

	public override void Execute()
	{
		foreach (var component in _connectedComponents)
		{
			component.IsPowered = true;
		}
	}

	#endregion

}
