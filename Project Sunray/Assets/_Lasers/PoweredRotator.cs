﻿using UnityEngine;

public class PoweredRotator : PoweredBase
{

	#region EditorFields

	[SerializeField]
	private Vector3 _rotationIncrements;

	#endregion

	#region Constructor

	/// <summary>
	/// Use this for initialization
	/// </summary>
	private void Start () 
	{
		
	}

	#endregion
	
	#region Methods

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	protected override void Update () 
	{
		base.Update();
	}

	/// <summary>
	/// Executes this component rotating abilities
	/// </summary>
	public override void Execute()
	{
		transform.rotation *= Quaternion.Euler(_rotationIncrements * Time.deltaTime);
	}

	#endregion

}
