﻿using System.Collections.Generic;
using UnityEngine;

public class QuadCopter : MonoBehaviour 
{

	#region EditorFields

	[SerializeField]
	private List<Transform> _engineList;

	[SerializeField]
	private float _maxHover;

	[SerializeField]
	private float _hoverHeight;

	[SerializeField]
	private float _acceleration;

	[SerializeField]
	private float _force;

	[SerializeField]
	private float _minMaxAngle;

	#endregion

	#region Fields

	private Rigidbody _rigidbody;

	#endregion

	#region Constructor

	/// <summary>
	/// Use this for initialization
	/// </summary>
	private void Start ()
	{
		_rigidbody = GetComponent<Rigidbody>();
	}

	#endregion
	
	#region Methods

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	private void Update ()
	{
		//Debug.Log(Input.GetAxis("Vertical"));
		_engineList[0].rotation = Quaternion.Euler(_engineList[0].rotation.eulerAngles.x, _engineList[0].rotation.eulerAngles.y, _engineList[0].rotation.eulerAngles.z + Input.GetAxis("Vertical") * _minMaxAngle );
		_engineList[1].rotation = Quaternion.Euler(_engineList[1].rotation.eulerAngles.x, _engineList[1].rotation.eulerAngles.y, _engineList[1].rotation.eulerAngles.z + Input.GetAxis("Vertical") * _minMaxAngle);
	}

	private void FixedUpdate()
	{
		for (var index = 0; index < _engineList.Count; index++)
		{
			var engine = _engineList[index];
			Ray ray = new Ray(engine.position, -engine.up);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, _hoverHeight))
			{
				float proportionalHeight = (1.0f - hit.distance) / _hoverHeight;
				Vector3 appliedHoverForce = engine.up * proportionalHeight * _force;
				//Because we have 4 engines
				appliedHoverForce /= 4.0f;
				Debug.DrawLine(engine.position, engine.position + (-engine.up * proportionalHeight), Color.green);
				Vector3 forwardDir = Vector3.zero;
				Vector3 turningDir = Vector3.zero;
				if (index == 0 || index == 1)
				{
					//forwardDir =(appliedHoverForce/4.0f) * Input.GetAxis("Vertical") * ((index == 0 || index == 1) ? 1.0f : -1.0f);
				}

				if (index == 0 || index == 2)
				{
					turningDir = appliedHoverForce/2.0f * Input.GetAxis("Horizontal") * ((index == 0 || index == 2) ? -1.0f : 1.0f);		
				}

				_rigidbody.AddForceAtPosition(appliedHoverForce + forwardDir + turningDir , engine.transform.position, ForceMode.Force);
			}
			else
			{
				float force = _rigidbody.mass * Mathf.Abs(Physics.gravity.y);
				force /= 5.0f;
				_rigidbody.AddForceAtPosition(engine.up * force, engine.transform.position, ForceMode.Force);
			}

		}

		//Ray ray = new Ray(transform.position, -transform.up);
		//RaycastHit hit;
		//if (Physics.Raycast(ray, out hit, _hoverHeight))
		//{
		//	float proportionalHeight = (_hoverHeight - hit.distance) / _hoverHeight;
		//	Vector3 appliedHoverForce = transform.up * proportionalHeight * _force;
		//	_rigidbody.AddForce(appliedHoverForce, ForceMode.Acceleration);
		//	//_rigidbody.AddForceAtPosition(transform.transform.position, appliedHoverForce / 4.0f, ForceMode.Force);
		//}


		//float force = _rigidbody.mass * Mathf.Abs(Physics.gravity.y);
		//foreach (var engine in _engineList)
		//{
		//	_rigidbody.AddForce(engine.up * (force/4.0f));
		//}
	}

	#endregion

}
