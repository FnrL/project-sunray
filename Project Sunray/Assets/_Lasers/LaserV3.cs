﻿using UnityEngine;

[ExecuteInEditMode]
public class LaserV3 : PoweredBase
{
	#region EditorFields

	[SerializeField]
	private float _maxDistance = 50f;

	[SerializeField]
	private GameObject _laserPrefab;

	[SerializeField]
	private bool _destroyOnUnpowered;

	#endregion

	#region Fields

	private LineRenderer _lineRenderer;

	#endregion

	#region Properties

	public LayerMask LayersToIgnore;

	public LaserV3 PowerSource { get; set; }

	#endregion

	#region Constructor

	/// <summary>
	/// Use this for initialization
	/// </summary>
	private void Start ()
	{
		_lineRenderer = GetComponent<LineRenderer>();
	}

	#endregion
	
	#region Methods

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	protected override void Update () 
	{
		base.Update();
	}

	public override void Execute()
	{
		RaycastHit hit;
		if (Physics.Raycast(_lineRenderer.transform.position, _lineRenderer.transform.forward, out hit, _maxDistance, ~LayersToIgnore))
		{
			_lineRenderer.SetPosition(1, Vector3.forward * hit.distance);
			//Reflector
			if (hit.collider.CompareTag("Reflector"))
			{
				bool childFound = false;
				foreach (var laser in hit.collider.GetComponentsInChildren<LaserV3>())
				{
					if (laser.PowerSource == this)
					{
						laser.transform.position = hit.point;
						laser.transform.rotation = Quaternion.LookRotation(Vector3.Reflect((hit.point - _lineRenderer.transform.position).normalized, hit.normal));
						Debug.DrawLine(hit.point, hit.point + hit.normal * 5, Color.magenta);
						Debug.DrawLine(hit.point, hit.point + Vector3.Reflect((hit.point - _lineRenderer.transform.position).normalized, hit.normal) * 5, Color.blue);
						laser.IsPowered = true;
						childFound = true;
						break;
					}
				}

				if (childFound == false)
				{
					SpawnLineRenderer(hit.collider.transform.gameObject);
				}
			}
			else //PoweredBase
			{
				PoweredBase pb = hit.collider.GetComponent<PoweredBase>();
				if (pb != null)
				{
					pb.IsPowered = true;
				}
			}
		}
		else
		{
			_lineRenderer.SetPosition(1, Vector3.forward * _maxDistance);
		}
	}

	protected override void LateUpdate()
	{
		if (IsPowered == false)
		{
			_lineRenderer.enabled = false;

			if (_destroyOnUnpowered)
			{
#if UNITY_EDITOR
				DestroyImmediate(gameObject);
#else
				Destroy(gameObject)
#endif		
			}
		}
		else
		{
			_lineRenderer.enabled = true;
		}

		base.LateUpdate();
	}

	private void SpawnLineRenderer(GameObject hitObj)
	{
		GameObject child = Instantiate(_laserPrefab);
		child.transform.parent = hitObj.transform;
		LaserV3 laser = child.GetComponent<LaserV3>();
		laser.IsPowered = true;
		laser.PowerSource = this;
		laser.LayersToIgnore = this.LayersToIgnore;
	}

#endregion


}
