﻿using UnityEngine;

[ExecuteInEditMode]
public class Laser : MonoBehaviour
{

	#region EditorFields

	[SerializeField] private float _maxDistance = 50f;

	[SerializeField]
	private string id;

	#endregion

	#region Fields

	private LineRenderer _lineRenderer;
	private bool _isAlive;
	private bool _wasAlive;

	#endregion

	#region Properties

	public bool IsAlive
	{
		set
		{
			_isAlive = value;
			if (_isAlive)
			{
				_wasAlive = true;
			}
			//_lineRenderer.enabled = _isAlive;
		}
		get { return _isAlive; }
	}

	#endregion

	#region Constructor

	private void Awake()
	{
		GameManager gm = GameManager.Instance;
	}

	/// <summary>
	/// Use this for initialization
	/// </summary>
	private void Start()
	{
		_lineRenderer = GetComponent<LineRenderer>();
	}

	#endregion

	#region Methods

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	private void Update()
	{
		if (id == "01")
		{
			int test = 5;
		}

		if (_isAlive)
		{
			RaycastHit hit;

			if (Physics.Raycast(_lineRenderer.transform.position, _lineRenderer.transform.forward, out hit, _maxDistance))
			{
				_lineRenderer.SetPosition(1,Vector3.forward * hit.distance);
				if (hit.collider.tag == "Reflector")
				{
					Transform laserChild = hit.collider.transform.Find("[LaserChild]");
					if (laserChild == null)
					{
						SpawnLineRenderer(hit.collider.transform.gameObject);
					}
					else
					{
						laserChild.position = hit.point;
						laserChild.rotation = Quaternion.LookRotation(Vector3.Reflect((hit.point - _lineRenderer.transform.position).normalized, hit.normal));
						Debug.DrawLine(hit.point, hit.point + hit.normal * 5, Color.red);
						Debug.DrawLine(hit.point, hit.point + Vector3.Reflect((hit.point - _lineRenderer.transform.position).normalized, hit.normal) * 5, Color.blue);

						laserChild.GetComponent<Laser>().IsAlive = true;							
					}
				}
			}
			else
			{
				_lineRenderer.SetPosition(1,Vector3.forward * _maxDistance);
			}
		}
	}

	private void LateUpdate()
	{
		if (_isAlive == false)
		{
			_lineRenderer.enabled = false;
		}
		else
		{
			_lineRenderer.enabled = true;
		}

		if (_wasAlive == false)
		{
			_isAlive = false;
		}
		_wasAlive = false;

	}

	private void SpawnLineRenderer(GameObject hitObj)
	{
		GameObject child = new GameObject("[LaserChild]");
		child.transform.parent = hitObj.transform;
		LineRenderer newLineRenderer = child.AddComponent<LineRenderer>();
		Laser laser = child.AddComponent<Laser>();
		laser.IsAlive = true;
		newLineRenderer.endWidth = _lineRenderer.endWidth;
		newLineRenderer.startWidth = _lineRenderer.startWidth;
		newLineRenderer.useWorldSpace = false;
	}

	//

	private void FilterLaser()
	{
		RaycastHit hit;
		if (Physics.Raycast(transform.position,transform.forward, out hit, 50))
		{
			Debug.Log("Object hit");
		}
	}



	#endregion

}
