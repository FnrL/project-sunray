﻿using UnityEngine;

[ExecuteInEditMode]
public class LaserV2 : MonoBehaviour 
{
	#region Fields

	private LineRenderer _lineRenderer;

	private int _hitCounter = 0;
	
	#endregion

	#region EditorFields

	[SerializeField]
	private float _maxDistance = 50;

	#endregion

	#region Constructor

	/// <summary>
	/// Use this for initialization
	/// </summary>
	private void Start ()
	{
		_lineRenderer = GetComponent<LineRenderer>();
	}

	#endregion
	
	#region Methods

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	private void Update()
	{
		_hitCounter = 0;
		_lineRenderer.positionCount = 1;

		//ShootBeam();

		ShootBeamFromInDir(transform.position, transform.forward);
	}

	private void ShootBeamFromInDir(Vector3 start, Vector3 dir)
	{
		_hitCounter++;
		_lineRenderer.positionCount++;
		RaycastHit hit;
		if (Physics.Raycast(start+ dir * 0.02f, dir, out hit, _maxDistance))
		{
			if (hit.collider.tag == "Reflector" )
			{	
				_lineRenderer.SetPosition(_hitCounter, WorldToLocal(hit.point));
				ShootBeamFromInDir(hit.point, Vector3.Reflect((hit.point - (transform.position + _lineRenderer.GetPosition(_hitCounter - 1))).normalized, hit.normal));		
			}
		}
		else
		{
			Debug.DrawLine(start, start + dir * 5, Color.blue);
			_lineRenderer.SetPosition(_hitCounter, _lineRenderer.GetPosition(_hitCounter - 1) + dir /** _maxDistance*/);
		}
	}

	private Vector3 WorldToLocal(Vector3 worldPos)
	{
		return transform.position - worldPos;
	}



	#endregion

}
