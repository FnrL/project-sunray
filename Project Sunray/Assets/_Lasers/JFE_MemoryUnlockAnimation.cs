﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class JFE_MemoryUnlockAnimation : MonoBehaviour 
{

	#region EditorFields

	/// <summary>
	/// Time it takes the animation to complete.
	/// </summary>
	[Tooltip("Time it takes the animation to complete.")]
	[SerializeField]
	private float _timeToComplete;

	/// <summary>
	/// Target position for the coins to lerp to.
	/// </summary>
	[Tooltip("Target position for the coins to lerp to.")]
	[SerializeField]
	private Transform _animationTarget;

	/// <summary>
	/// Movement speed used to lerp to the animation target during the animaton.
	/// </summary>
	[Tooltip("Movement speed used to lerp to the animation target during the animaton.")]
	[SerializeField]
	private AnimationCurve _movementSpeed = new AnimationCurve(new Keyframe(0, 0, -1, 1), new Keyframe(1, 1,1, -1));

	/// <summary>
	/// How fast the coins are rotating around the central orbiting point.
	/// </summary>
	[Tooltip("How fast the coins are rotating around the central orbiting point.")]
	[SerializeField]
	private AnimationCurve _angularSpeed = new AnimationCurve(new Keyframe(0, 0, -1, 1), new Keyframe(1, 1, 1, -1));
	
	/// <summary>
	/// Content to be unlocked.
	/// </summary>
	[Tooltip("Content to be unlocked.")]
	[SerializeField]
	private GameObject _content;

	/// <summary>
	/// Gameobject that is show when memory is locked and get hidden when the memory is being unlocked.
	/// </summary>
	[SerializeField]
	[Tooltip("Gameobject that is show when memory is locked and get hidden when the memory is being unlocked.")]
	private GameObject _preview;

	/// <summary>
	/// The event that fires when the memory is unlocked.
	/// </summary>
	[Header("Events")]
	[SerializeField]
	[Tooltip("The event that fires when the memory is unlocked.")]
	private UnityEvent _memoryUnlock;

	#endregion

	#region Fields

	/// <summary>
	/// internal timer used for tracking animation completion
	/// </summary>
	private float _timer;

	/// <summary>
	/// List of all coins in the animation
	/// </summary>
	private List<JFE_MemoryCoinVisual> _coinList = new List<JFE_MemoryCoinVisual>();

	/// <summary>
	/// Current center of orbit.
	/// </summary>
	private Vector3 _currentTargetPosition;

	#endregion

	#region Constructor

	/// <summary>
	/// Carry over the list of coins from the memory
	/// </summary>
	/// <param name="coinList">List of filled coins orbiting the memory</param>
	public void Init(List<JFE_MemoryCoinVisual> coinList)
	{
		_coinList = coinList;
	}

	/// <summary>
	/// Use this for initialization
	/// </summary>
	private void Start ()
	{
		_currentTargetPosition = transform.position;
		_preview.SetActive(true);
		_content.SetActive(false);
		AudioManager.Wwise_SendEvent(gameObject, AudioManager.EVENTS.OBJECT_ORB_SPEEDUP);
	}

	#endregion
	
	#region Methods

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	private void Update ()
	{
		_timer += Time.deltaTime;
		if (_timer >= _timeToComplete)
		{
			UnlockMemory();
		}
	}


	private void FixedUpdate()
	{
		//Increase the centripedal force so the coins gravitate towards the center
		foreach (var coin in _coinList)
		{
			_currentTargetPosition = Vector3.Lerp(transform.position, _animationTarget.position, _movementSpeed.Evaluate(_timer / _timeToComplete));
			float desiredRadius = Mathf.Lerp(coin.Radius, 0.05f, _angularSpeed.Evaluate(_timer / _timeToComplete));
			Rigidbody coinRigidbody = coin.GetComponent<Rigidbody>();
			Vector3 centripedalForce = (_currentTargetPosition - coin.transform.position).normalized * ((coinRigidbody.velocity.magnitude * coinRigidbody.velocity.magnitude) / desiredRadius);

			coinRigidbody.AddForce(centripedalForce, ForceMode.Force);
		}
	}

	/// <summary>
	/// Unlock the memory
	/// </summary>
	private void UnlockMemory()
	{
		if (_memoryUnlock != null)
		{
			_memoryUnlock.Invoke();
		}

		AudioManager.Wwise_SendEvent(gameObject, AudioManager.EVENTS.OBJECT_ORB_END);
		//_preview.SetActive(false);
		//_content.SetActive(true);
		foreach (var coin in _coinList)
		{
			Destroy(coin.gameObject);
		}
		_coinList.Clear();
		this.enabled = false;
	}

	#endregion

}
