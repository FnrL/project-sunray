﻿
public interface IPowered
{
	bool IsPowered { get; set; }
	void Execute();
}
