﻿using System.Collections.Generic;
using UnityEngine;

public abstract class PoweredBase : MonoBehaviour 
{

	#region Fields

	private bool _isPowered;
	private bool _wasPowered;

	#endregion

	#region Properties

	public bool IsPowered
	{
		set
		{
			_isPowered = value;
			if (_isPowered)
			{
				_wasPowered = true;
			}
		}
		get { return _isPowered; }
	}

	#endregion

	#region Constructor

	/// <summary>
	/// Use this for initialization
	/// </summary>
	private void Start () 
	{
		
	}

	#endregion
	
	#region Methods

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	protected virtual void Update () 
	{
		if (IsPowered)
		{
			Execute();
		}
	}

	protected virtual void LateUpdate()
	{
		if (_wasPowered == false)
		{
			_isPowered = false;
		}
		_wasPowered = false;
	}

	protected void VisualizeConnections(List<PoweredBase> connections)
	{
		if (connections == null || connections.Count == 0)
		{
			return;
		}

		foreach (var comp in connections)
		{
			if (comp == null)
				continue;

			float distance = (transform.position - comp.transform.position).magnitude;
			if (distance < 0.5f)
			{
				Gizmos.color = comp.IsPowered && this.IsPowered ? Color.green : Color.red;
				Gizmos.DrawSphere(comp.transform.position, 0.2f);
			}
			else
			{
				Debug.DrawLine(transform.position, comp.transform.position, comp.IsPowered && this.IsPowered ? Color.green : Color.red);
			}
		}
	}

	/// <summary>
	/// Method that hold all the logic to current powered object
	/// </summary>
	public abstract void Execute();

	#endregion

}
