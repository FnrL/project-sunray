﻿using UnityEngine;

public class PoweredRigidBodyRotator : PoweredBase
{


	#region EditorFields

	[SerializeField]
	private Vector3 _torque;

	#endregion

	#region Fields

	private Rigidbody _rb;

	#endregion

	#region Constructor

	/// <summary>
	/// Use this for initialization
	/// </summary>
	private void Start ()
	{
		_rb = GetComponent<Rigidbody>();
	}

	#endregion
	
	#region Methods

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	private void FixedUpdate () 
	{
		base.Update();
	}

	public override void Execute()
	{
		_rb.AddTorque(_torque,ForceMode.VelocityChange);
	}

	#endregion

}
