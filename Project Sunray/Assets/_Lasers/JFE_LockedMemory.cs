﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using VRTK;

[RequireComponent(typeof(BoxCollider))]
public class JFE_LockedMemory : MonoBehaviour , IUniqueIdentifier
{

	#region Editor Fields

	/// <summary>
	/// Amount of coins needed to unlock this memory, they get generated on runtime.
	/// </summary>
	[Tooltip("Amount of coins needed to unlock this memory.")]
	[SerializeField]
	private int _coinsNeeded;

	/// <summary>
	/// Prefab of the coin that gets generated on runtime.
	/// </summary>
	[Tooltip("Prefab of the coin that gets generated on runtime.")]
	[SerializeField]
	private GameObject _coinPrefab;

	/// <summary>
	/// The distance the coins float to the memory center.
	/// </summary>
	[Tooltip("The distance the coins float to the memory center.")]
	[SerializeField]
	private float _radius;

	/// <summary>
	/// Coin rotation speed around the memory center.
	/// </summary>
	[Tooltip("Coin rotation speed around the memory center.")]
	[SerializeField]
	private float _forwardRotationSpeed;

	/// <summary>
	/// Random between 0 and 1 gets multiplied with the max angular speed.
	/// </summary>
	[SerializeField]
	[Tooltip("Maximum angular speed that can be given to a coin.")]
	private float _maxAngularSpeed;

	/// <summary>
	/// Speed the coins use when lerping to their ghoslty counter parts. This gets multiplied with time since coin has been lerping.
	/// </summary>
	[SerializeField]
	[Tooltip("Speed the coins use when lerping to their ghoslty counter parts. This gets multiplied with time since coin has been lerping.")]
	[Range(0.0f,1.0f)]
	private float _lerpToSpeed;

	/// <summary>
	/// Memory content that can be unlocked.
	/// </summary>
	[SerializeField]
	[Tooltip("Memory content that can be unlocked.")]
	private GameObject _content;

	/// <summary>
	/// Gameobject that is show when memory is locked and get hidden when the memory is being unlocked.
	/// </summary>
	[SerializeField]
	[Tooltip("Gameobject that is show when memory is locked and get hidden when the memory is being unlocked.")]
	private GameObject _preview;

	/// <summary>
	/// Offsets each variable with a random to give the memory a more chaotic effect.
	/// </summary>
	[SerializeField]
	[Tooltip("Offsets each variable with a random to give the memory a more chaotic effect.")]
	private bool _useRandomness;

	/// <summary>
	/// The particle effect that plays when submiting a coin to the Locked memory.
	/// </summary>
	[Header("Particles")]
	[SerializeField]
	[Tooltip("The particle effect that plays when submiting a coin to the Locked memory.")]
	private GameObject _coinSubmitFeedback;

	/// <summary>
	/// The time it takes for the coin submit feedback particle to be destroyed.
	/// </summary>
	[SerializeField]
	[Tooltip("The time it takes for the coin submit feedback particle to be destroyed.")]
	private float _feedbackDestroyTime;

	[SerializeField]
	private UnityEvent _onCoinSubmitted;

	[SerializeField]
	private UnityEvent _onCoinsCollected;

	[Header("Debug")]
	[SerializeField]
	[Tooltip("Draw debug lines for testing purposes.")]
	private bool _drawDebugLines;

	#if UNITY_EDITOR
	[SerializeField]
	[Tooltip("completes the orb without the need of adding coins (like a button)")]
	private bool _autoCompleteOrb = false;
	#endif

	[SerializeField]
	[Tooltip("The amount of coins currently in the orb (used in the savedata)")]
	private int _currentCoinCount = 0;

	[SerializeField]
	private GameObject _itemInSceneWhenNotCompleted;

	[SerializeField]
	private GameObject _itemInSceneWhenCompleted;

	#endregion

	#region Fields

	/// <summary>
	/// List of generated coins orbiting around this.transform
	/// </summary>
	private List<JFE_MemoryCoinVisual> _coinList = new List<JFE_MemoryCoinVisual>();

	/// <summary>
	/// List of coins that are put into this memory and are lerping to their corresponding ghost coin position
	/// </summary>
	private List<JFE_MemoryCoin> _slidingCoinList = new List<JFE_MemoryCoin>();

	/// <summary>
	/// Index position at which this memory is filled
	/// </summary>
	private int _filledIndex;

	#endregion

	#region Properties

	/// <summary>
	/// Amount of coins that are submitted to this memory.
	/// </summary>
	public int SubmittedAmountOfCoins { get { return _filledIndex;} }

	/// <summary>
	/// Called when all coins are collected
	/// </summary>
	public UnityEvent OnCoinsCollected
	{
		get { return _onCoinsCollected; }
	}

	#endregion

	#region IUniqueIdentifier

	[SerializeField]
	private int _uniqueID;

	public int UniqueID
	{
		get { return _uniqueID; }
		set { _uniqueID = value; }
	}
	public object Value
	{
		get { return _currentCoinCount; }
		set { _currentCoinCount = System.Convert.ToInt32(value); }
	}

	#endregion

	#region Constructor

	/// <summary>
	/// Use this for initialization
	/// </summary>
	private void Start()
	{
		// ActivateMemoryItemInScene(false);

		if (_preview != null)
		{
			_preview.SetActive(true);
		}
		_content.SetActive(false);

		GenerateOrbitingCoins();

		if (_coinsNeeded > 0) WwiseGlobalController.MainBankLoaded += OnMainBankLoaded;
	}

	#endregion

	#region Methods

	private void OnMainBankLoaded()
	{
		// Send orb loop event to Wwise if coins are needed (which means the orb will be visible)
		AudioManager.Wwise_SendEvent(_content.GetComponent<JFE_MemoryUnlockAnimation>().gameObject, AudioManager.EVENTS.OBJECT_ORB_LOOP);
		WwiseGlobalController.MainBankLoaded -= OnMainBankLoaded;
	}

	/// <summary>
	/// Generates all the needed orbiting coins for this memory.
	/// </summary>
	private void GenerateOrbitingCoins()
	{
		GameManager.Instance.SavingLoadingManager.LoadPersistanObjectValue(this);
		int coinsToBeFilled = _currentCoinCount;
		_filledIndex = _currentCoinCount;
		for (int i = 0; i < _coinsNeeded; i++)
		{
			//Place coin at random position in orbit of the Memory
			GameObject coin = Instantiate(_coinPrefab, transform);
			float coinRadius = _radius + (_useRandomness ? Random.Range(-0.2f, 0.2f) : 0.0f);
			coin.transform.localPosition =
				new Vector3(Random.Range(-1.0f, 1.0f), _useRandomness ? Random.Range(-0.5f, 0.5f) : 0, Random.Range(-1.0f, 1.0f)).normalized * coinRadius;
			coin.transform.LookAt(Vector3.Cross(transform.position - coin.transform.position, Vector3.up));
			JFE_MemoryCoinVisual memoryCoin = coin.GetComponent<JFE_MemoryCoinVisual>();
			//memoryCoin.IsInventoryCoin = false;
			memoryCoin.Radius = coinRadius;
			_coinList.Add(memoryCoin);

			Vector3 forward = Vector3.Cross(transform.position - coin.transform.position, Vector3.up).normalized * _forwardRotationSpeed;
			if (_useRandomness)
			{
				forward *= UnityEngine.Random.Range(0.5f, 1.5f);
			}

			Rigidbody coinRigidbody = coin.GetComponent<Rigidbody>();
			coinRigidbody.angularDrag = 0.0f;
			coinRigidbody.useGravity = false;
			coinRigidbody.AddForce(forward, ForceMode.Acceleration);
			coinRigidbody.angularVelocity = new Vector3(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f)).normalized * _maxAngularSpeed;
			if(coinsToBeFilled != 0)
			{
				--coinsToBeFilled;
				memoryCoin.IsFilled = true;
			}
			else
			{
				memoryCoin.IsFilled = false;
			}
		}

		if (_coinsNeeded == 0)
		{
			ForceUnlockMemory();
			this.gameObject.SetActive(false);
		}
		else if(_currentCoinCount == _coinsNeeded)
		{
			// ForceUnlockMemory();
			this.gameObject.SetActive(false);
		}
	}

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	private void Update()
	{
#if UNITY_EDITOR
		if (_autoCompleteOrb)
		{
			_autoCompleteOrb = false;
			ForceUnlockMemory();
		}
#endif

		//Sliding in coins
		for (var i = 0; i < _slidingCoinList.Count; i++)
		{
			if (_slidingCoinList[i] == null)
				continue;

			//Smoothing out the entry vector and gradually aligning it with the ghost coin movement vector
			Vector3 current = _slidingCoinList[i].Rigidbody.velocity;
			Vector3 desired = (_coinList[_slidingCoinList[i].TargetIndex].transform.position - _slidingCoinList[i].transform.position);

			if (_drawDebugLines)
			{
				Debug.DrawRay(_slidingCoinList[i].transform.position, current.normalized, Color.yellow);
				Debug.DrawRay(_slidingCoinList[i].transform.position, desired.normalized, Color.red);
			}

			_slidingCoinList[i].Rigidbody.velocity =
				Vector3.Lerp(current.normalized, desired.normalized, current.magnitude * _lerpToSpeed * _slidingCoinList[i].SlideInTime)
					.normalized * Mathf.Lerp(_slidingCoinList[i].EntryAndTargetSpeed.x, _slidingCoinList[i].EntryAndTargetSpeed.y + 0.2f,
					_slidingCoinList[i].SlideInTime / (Vector3.Distance(_slidingCoinList[i].transform.position, _coinList[_slidingCoinList[i].TargetIndex].transform.position) /
					                    _slidingCoinList[i].Rigidbody.velocity.magnitude)); // adding + 0.2f so we make sure we actually overtake the ghost coin

			//OLD METHOD
			//Lerp coins to new position closer to target position (Ghostly counter part)
			//coin.transform.position = Vector3.Lerp(coin.transform.position, _coinList[coin.TargetIndex].transform.position, _lerpToSpeed * coin.SlideInTime);
			//Debug.Log(coin.GetComponent<Rigidbody>().velocity.magnitude);

			//Distance check
			float distance = Vector3.Distance(_slidingCoinList[i].transform.position, _coinList[_slidingCoinList[i].TargetIndex].transform.position);
			if (distance < 0.020f)
			{
				SumbitCoin(_slidingCoinList[i]);
			}
		}
	}

	private void FixedUpdate()
	{
		//Add centripedal force to each coin to keep them in orbit
		for (var i = 0; i < _coinList.Count; i++)
		{
			Rigidbody coinRigidbody = _coinList[i].Rigidbody;
			Vector3 forward = Vector3.Cross(transform.position - _coinList[i].transform.position, Vector3.up).normalized *_forwardRotationSpeed;
			Vector3 centripedalForce = (transform.position - _coinList[i].transform.position).normalized *
			                           ((coinRigidbody.velocity.magnitude * coinRigidbody.velocity.magnitude) / _coinList[i].Radius);
			coinRigidbody.AddForce(centripedalForce, ForceMode.Force);

			//Debug lines
			if (_drawDebugLines)
			{
				Debug.DrawRay(_coinList[i].transform.position, forward.normalized, Color.blue);
				Debug.DrawRay(_coinList[i].transform.position, centripedalForce.normalized, Color.red);
			}
		}
	}

	/// <summary>
	/// On trigger enter.
	/// </summary>
	/// <param name="other">other collider</param>
	private void OnTriggerEnter(Collider other)
	{
		JFE_MemoryCoin coin = other.GetComponent<JFE_MemoryCoin>();
		if (coin != null && coin.IsSlidingIn == false)
		{
			//If the coin is lerping back to pouch we dont want it.
			if (coin.IsLerping)
				return;

			//Do we already have enough coins?
			if (_filledIndex >= _coinsNeeded)
				return;

			Rigidbody coinRigidbody = coin.Rigidbody;

			//Release coin from hand
			VRTK_InteractableObject interactableObject = coin.GetComponent<VRTK_InteractableObject>();
			if (interactableObject != null)
			{
				if (interactableObject.IsGrabbed())
				{
					interactableObject.GetGrabbingObject().GetComponent<VRTK_InteractGrab>().ForceRelease();
					coinRigidbody.velocity = (transform.position - coin.transform.position).normalized * 2.0f;
					coinRigidbody.AddRelativeTorque(new Vector3(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f)).normalized * _maxAngularSpeed, ForceMode.VelocityChange);
				}

				interactableObject.Ungrabbed();
				interactableObject.isGrabbable = false;
				interactableObject.isUsable = false;
			}

			//coinRigidbody.isKinematic = true;
			coinRigidbody.useGravity = false;

			coin.IsSlidingIn = true;
			coin.IsDecaying = false;
			coin.TargetIndex = _filledIndex;
			_filledIndex++;
			_slidingCoinList.Add(coin);
			coin.EntryAndTargetSpeed = new Vector2(coinRigidbody.velocity.magnitude,_coinList[coin.TargetIndex].Rigidbody.velocity.magnitude);
		}
	}

	/// <summary>
	/// Slot coin into its fitting ghostly counter part
	/// </summary>
	/// <param name="coin">Coin that has reached it orbit counter part</param>
	private void SumbitCoin(JFE_MemoryCoin coin)
	{
		// Save the coin count and remove from the inventory
		_currentCoinCount++;
		GameManager.Instance.SavingLoadingManager.GameData.RemoveCoin();
		if(coin.ParentCoinStack != JFE_CoinStack.CoinStackInHand)
		{
			if (JFE_CoinStack.CoinStackInHand != null)
			{
				JFE_CoinStack.CoinStackInHand.RemoveCoin();
			}
		}
		GameManager.Instance.SavingLoadingManager.SavePersistantObjectValue(this);

		_coinList[coin.TargetIndex].IsFilled = true;
		_coinList[coin.TargetIndex].Rigidbody.angularVelocity = coin.Rigidbody.angularVelocity;
		GameObject particleFeedback = Instantiate(_coinSubmitFeedback, _coinList[coin.TargetIndex].transform);
		Destroy(particleFeedback, _feedbackDestroyTime);
		Destroy(coin.gameObject);

        // Play coin sound
        AudioManager.Wwise_SendEvent(gameObject, AudioManager.EVENTS.OBJECT_COIN_IN_ORB);

		//Unity event on coin submitted.
		if (_onCoinSubmitted != null)
		{
			_onCoinSubmitted.Invoke();
		}

		//Check if all coins are in place and if the memory is completed
		bool completed = true;
		foreach (var c in _coinList)
		{
			if (c.IsFilled == false)
			{
				completed = false;
				break;
			}
		}
		//Activate the memory content and clean up variables
		if (completed)
		{
			UnlockMemory();
		}
	}

	private void UnlockMemory(bool withSound = false)
	{
		if (_preview != null)
		{
			_preview.SetActive(false);
		}
		_content.SetActive(true);
		JFE_MemoryUnlockAnimation unlockAnim = _content.GetComponent<JFE_MemoryUnlockAnimation>();
		unlockAnim.enabled = true;
		unlockAnim.Init(_coinList);

		if (withSound) AudioManager.Wwise_SendEvent(gameObject, AudioManager.EVENTS.OBJECT_ORB_END);

		_onCoinsCollected.Invoke();
		this.enabled = false;
		ActivateMemoryItemInScene(true);
	}

	/// <summary>
	/// Get's called from a unity event
	/// </summary>
	public void ForceUnlockMemory()
	{
		UnlockMemory(true);
	}

	public void ActivateMemoryItemInScene(bool value)
	{
		if(_itemInSceneWhenCompleted == null)
		{
			Debug.Log("_itemInSceneWhenCompleted  == null");
			return;
		}
		if(_itemInSceneWhenNotCompleted == null)
		{
			Debug.Log("_itemInSceneWhenCompleted  == null");
			return;
		}

		_itemInSceneWhenCompleted.SetActive(value);
		_itemInSceneWhenNotCompleted.SetActive(!value);
	}

	#endregion

}


