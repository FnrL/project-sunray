﻿using System.Collections.Generic;
using UnityEngine;

public class HoverBoard : MonoBehaviour 
{
	#region EditorFields

	[SerializeField]
	private float _hoverHeight;

	[SerializeField]
	private float _force;

	[SerializeField]
	private float _baseForce;

	[SerializeField]
	private bool _useProportianalBaseForce;

	[SerializeField]
	private List<Transform> _points;

	[SerializeField]
	private bool _localUp;

	[Header("Stabilazation")]
	[SerializeField]
	private float _speed;

	[SerializeField]
	private float _stability;

	[SerializeField]
	private float _steeringMul = 1.0f;

	[SerializeField]
	private float _forwardMul = 1.0f;

	[SerializeField]
	private float _maxForwardLeanAngle;

	[SerializeField]
	private float _maxSideLeanAngle;

	#endregion

	#region Fields

	private Rigidbody _rigidbody;

	private Vector3 _startPos;

	private Quaternion _startRot;

	private float _actualHorizontal;

	#endregion

	#region Constructor

	/// <summary>
	/// Use this for initialization
	/// </summary>
	private void Start ()
	{
		_rigidbody = GetComponent<Rigidbody>();
		_startPos = transform.position;
		_startRot = transform.rotation;
	}

	#endregion
	
	#region Methods

	private void Update()
	{
		if (Input.GetKeyUp(KeyCode.R))
		{
			transform.position = _startPos;
			transform.rotation = _startRot;
			_rigidbody.velocity = Vector3.zero;
			_rigidbody.angularVelocity = Vector3.zero;
		}

		float gravity = 0.33f;
		if (Input.GetKey(KeyCode.Q))
		{
			_actualHorizontal += gravity;
			if (_actualHorizontal > 1.0f)
			{
				_actualHorizontal = 1.0f;
			}
		}
		else if (Input.GetKey(KeyCode.E))
		{
			_actualHorizontal -= gravity;
			if (_actualHorizontal < -1.0f)
			{
				_actualHorizontal = -1.0f;
			}
		}
		else
		{
			_actualHorizontal = Mathf.Lerp(_actualHorizontal, 0, 0.8f);
		}
	}

	private Vector3 _currRot = Vector3.zero;

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	private void FixedUpdate () 
	{
		foreach (var point in _points)
		{
			Ray ray = new Ray(transform.position, -transform.up);
			RaycastHit hit;
			Debug.DrawLine(point.position, point.position + -transform.up * _hoverHeight, Color.blue);
			//Push of force when above land
			if (Physics.Raycast(ray, out hit, _hoverHeight))
			{
				float proportionalHeight = (_hoverHeight - hit.distance) / _hoverHeight;
				Vector3 appliedHoverForce = _localUp ? Vector3.up : transform.up * proportionalHeight * _force;
				_rigidbody.AddForceAtPosition(appliedHoverForce,point.position, ForceMode.Force);
			}
			//Base force, just shy of cancling out gravity.
			_rigidbody.AddForceAtPosition(_localUp ? Vector3.up : transform.up * _baseForce * (_useProportianalBaseForce ? hit.distance / _hoverHeight : 1.0f), point.position, ForceMode.Force);	
			Debug.DrawRay(transform.position, _localUp ? Vector3.up : transform.up, _localUp ? Color.green : Color.cyan);
		}

		//TODO: this is a flipping prototype
		if (Input.GetKey(KeyCode.LeftShift) == false)
		{
			//Forward
			_rigidbody.AddTorque(transform.right * Input.GetAxisRaw("Vertical") * _forwardMul * GetLimitingFactor(transform.rotation.eulerAngles.x, _maxForwardLeanAngle));
		}
		else if(Input.GetKey(KeyCode.LeftShift))
		{
			//Forward
			_rigidbody.AddTorque(transform.right * Input.GetAxisRaw("Vertical") * _forwardMul * (Mathf.Abs(transform.rotation.eulerAngles.x) / _maxForwardLeanAngle));
		}


		//Steering
		_rigidbody.AddTorque(transform.up * _steeringMul * Input.GetAxisRaw("Horizontal"));
		//Sideways
		_rigidbody.AddTorque(transform.forward * _actualHorizontal * GetLimitingFactor(transform.rotation.eulerAngles.z, _maxSideLeanAngle));

		Vector3 predictedUp = Quaternion.AngleAxis(_rigidbody.angularVelocity.magnitude * Mathf.Rad2Deg * _stability / _speed,
			                      _rigidbody.angularVelocity) * transform.up;
		Vector3 torqueVector = Vector3.Cross(predictedUp, Vector3.up);

		Vector3 torqueVectorZ = Vector3.Project(torqueVector, transform.forward);
		Vector3 torqueVectorX = Vector3.Project(torqueVector, transform.right);

		//X Axis
		_rigidbody.AddTorque(torqueVectorX * _speed * _speed * (1.0f - Mathf.Abs(Input.GetAxisRaw("Vertical"))));
		//Z Axis
		_rigidbody.AddTorque(torqueVectorZ * _speed * _speed * (1.0f - Mathf.Abs(_actualHorizontal)));	
		//Stabilization
		
	}

	private float GetLimitingFactor(float anglToLimit, float angleLimit)
	{
		return Mathf.Clamp(1.0f - Mathf.Abs(Mathf.DeltaAngle(anglToLimit, 0)) % 360.0f / angleLimit,
			0.0f, 1.0f);
	}



	#endregion

}
