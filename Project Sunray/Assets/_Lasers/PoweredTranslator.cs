﻿using System.Runtime.Serialization;
using UnityEngine;

public class PoweredTranslator : PoweredBase
{
	#region EditorFields

	[SerializeField]
	private bool _isLocalSpace;

	[SerializeField]
	private Vector3 _translationPosition;

	/// <summary>
	/// If true then when this component is unpowered it will return to its default state again.
	/// </summary>
	[SerializeField]
	private bool _resetToDefault;

	[SerializeField]
	private float _timeToMove;

	#endregion

	#region Fields

	private Vector3 _startingPos;

	private float _timer;

	#endregion

	#region Constructor

	/// <summary>
	/// Use this for initialization
	/// </summary>
	private void Start ()
	{
		_startingPos = transform.position;
		if (_timeToMove == 0.0f)
		{
			Debug.LogWarning("_timeToMove equals 0 -> This has been set to a default 1");
		}
	}

	#endregion
	
	#region Methods

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	protected override void Update () 
	{
		base.Update();

		if (IsPowered && _timer < _timeToMove)
		{
			_timer += Time.deltaTime;
			if (_timer > _timeToMove)
			{
				_timer = _timeToMove;
			}
		}
		else if (IsPowered == false && _resetToDefault)
		{
			_timer -= Time.deltaTime;
			if (_timer < 0.0f)
			{
				_timer = 0.0f;
			}
		}

		if (_resetToDefault & IsPowered == false)
		{
			transform.position = Vector3.Lerp(_startingPos, _isLocalSpace ? _startingPos + _translationPosition :  _translationPosition, _timer / _timeToMove);
		}
	}

	public override void Execute()
	{
		if (_timer < _timeToMove)
		{
			transform.position = Vector3.Lerp(_startingPos, _isLocalSpace ? _startingPos + _translationPosition : _translationPosition, _timer / _timeToMove);
		}
	}

	#endregion
}
