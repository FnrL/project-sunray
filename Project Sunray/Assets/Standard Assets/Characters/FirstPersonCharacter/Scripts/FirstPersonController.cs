using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;

namespace UnityStandardAssets.Characters.FirstPerson
{
    [RequireComponent(typeof (CharacterController))]
    [RequireComponent(typeof (AudioSource))]
    public class FirstPersonController : MonoBehaviour
    {

	    #region Types

	    private enum ViewState
	    {
			FirstPerson,
			ThirdPerson
	    }

		#endregion

	    #region EditorFields

        [SerializeField] private bool _isWalking;
        [SerializeField] private float _walkSpeed;
        [SerializeField] private float _runSpeed;
        [SerializeField] [Range(0f, 1f)] private float _runstepLenghten;
        [SerializeField] private float _jumpSpeed;
        [SerializeField] private float _stickToGroundForce;
        [SerializeField] private float _gravityMultiplier;
        [SerializeField] private MouseLook _mouseLook;
        [SerializeField] private bool _useFovKick;
        [SerializeField] private FOVKick _fovKick = new FOVKick();
        [SerializeField] private bool _useHeadBob;
        [SerializeField] private CurveControlledBob _headBob = new CurveControlledBob();
        [SerializeField] private LerpControlledBob _jumpBob = new LerpControlledBob();
        [SerializeField] private float _stepInterval;
        [SerializeField] private AudioClip[] _footstepSounds;    // an array of footstep sounds that will be randomly selected from.
        [SerializeField] private AudioClip _jumpSound;           // the sound played when character leaves the ground.
        [SerializeField] private AudioClip _landSound;           // the sound played when character touches back on ground.    

	    [Header("Camera Settings")]
	    [SerializeField]
	    private Transform _thirdPersonParent;

		[SerializeField]
	    private Transform _firstPersonParent;

		#endregion

		#region Fields

		private ViewState _currentViewState = ViewState.FirstPerson;
		private Camera _camera;
        private bool _jump;
        private float _yRotation;
        private Vector2 _input;
        private Vector3 _moveDir = Vector3.zero;
        private CharacterController _characterController;
        private CollisionFlags _collisionFlags;
        private bool _previouslyGrounded;
        private Vector3 _originalCameraPosition;
        private float _stepCycle;
        private float _nextStep;
        private bool _jumping;
        private AudioSource _audioSource;
	    
	    #endregion

	    #region Properties

	    public bool LockCamera;

	    #endregion

		#region Methods

		void UpdateAnimator(Vector3 move)
		{
			//// update the animator parameters
			//_animator.SetFloat("Forward", _moveDir.z, 0.1f, Time.deltaTime);
			//_animator.SetFloat("Turn", Mathf.Atan2(move.x, move.z)/**/, 0.1f, Time.deltaTime);
			////_animator.SetBool("Crouch", _crouching);
			//_animator.SetBool("OnGround", _previouslyGrounded);
			////if (!m_IsGrounded)
			////{
			////	_animator.SetFloat("Jump", _rigidbody.velocity.y);
			////}

			//// calculate which leg is behind, so as to leave that leg trailing in the jump animation
			//// (This code is reliant on the specific run cycle offset in our animations,
			//// and assumes one leg passes the other at the normalized clip times of 0.0 and 0.5)
			//float runCycle =
			//	Mathf.Repeat(
			//		_animator.GetCurrentAnimatorStateInfo(0).normalizedTime + 0.2f, 1);
			//float jumpLeg = (runCycle < 0.5f ? 1 : -1) * _moveDir.z;
			////if (m_IsGrounded)
			////{
			////	_animator.SetFloat("JumpLeg", jumpLeg);
			////}

			//// the anim speed multiplier allows the overall speed of walking/running to be tweaked in the inspector,
			//// which affects the movement speed because of the root motion.
			////if (m_IsGrounded && move.magnitude > 0)
			////{
			////	_animator.speed = m_AnimSpeedMultiplier;
			////}
			////else
			////{
			//	// don't use that while airborne
			//	_animator.speed = 1;
			////}
		}

		// Use this for initialization
		private void Start()
        {
			_characterController = GetComponent<CharacterController>();
			_camera = Camera.main;
			_originalCameraPosition = _camera.transform.localPosition;
			_fovKick.Setup(_camera);
			_headBob.Setup(_camera, _stepInterval);
			_stepCycle = 0f;
			_nextStep = _stepCycle/2f;
			_jumping = false;
			_audioSource = GetComponent<AudioSource>();
			_mouseLook.Init(transform , _camera.transform);
        }


        // Update is called once per frame
        private void Update()
		{ 
			if (Input.GetKeyUp(KeyCode.F1))
			{
		        ToggelViewState();
	        }

			RotateView();
			// the jump state needs to read here to make sure it is not missed
			if (!_jump)
			{
				_jump = CrossPlatformInputManager.GetButtonDown("Jump");
			}
			
			if (!_previouslyGrounded && _characterController.isGrounded)
			{
			    StartCoroutine(_jumpBob.DoBobCycle());
			    PlayLandingSound();
			    _moveDir.y = 0f;
			    _jumping = false;
			}
			if (!_characterController.isGrounded && !_jumping && _previouslyGrounded)
			{
			    _moveDir.y = 0f;
			}
			
			_previouslyGrounded = _characterController.isGrounded;
        }

	    private void ToggelViewState()
		{
			switch (_currentViewState)
			{
				case ViewState.FirstPerson:
					_currentViewState = ViewState.ThirdPerson;
					_camera.transform.position = _thirdPersonParent.position;
					_camera.transform.localRotation = _thirdPersonParent.localRotation;
					break;
				case ViewState.ThirdPerson:
					_currentViewState = ViewState.FirstPerson;
					_camera.transform.position = _firstPersonParent.position;
					_camera.transform.localRotation = _firstPersonParent.localRotation;
					break;
			}
		}

        private void FixedUpdate()
        {
            float speed;
            GetInput(out speed);
            // always move along the camera forward as it is the direction that it being aimed at
            Vector3 desiredMove = _camera.transform.forward*_input.y + _camera.transform.right*_input.x;

            // get a normal for the surface that is being touched to move along it
            RaycastHit hitInfo;
            Physics.SphereCast(transform.position, _characterController.radius, Vector3.down, out hitInfo,
                               _characterController.height/2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
            desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

            _moveDir.x = desiredMove.x*speed;
            _moveDir.z = desiredMove.z*speed;


            if (_characterController.isGrounded)
            {
                _moveDir.y = -_stickToGroundForce;

                if (_jump)
                {
                    _moveDir.y = _jumpSpeed;
                    PlayJumpSound();
                    _jump = false;
                    _jumping = true;
                }
            }
            else
            {
                _moveDir += Physics.gravity*_gravityMultiplier*Time.fixedDeltaTime;
            }
            _collisionFlags = _characterController.Move(_moveDir*Time.fixedDeltaTime);


	        //UpdateAnimator(_moveDir);
			ProgressStepCycle(speed);
            UpdateCameraPosition(speed);

            _mouseLook.UpdateCursorLock();
        }

        private void PlayJumpSound()
        {
            _audioSource.clip = _jumpSound;
            _audioSource.Play();
        }

        private void ProgressStepCycle(float speed)
        {
            if (_characterController.velocity.sqrMagnitude > 0 && (_input.x != 0 || _input.y != 0))
            {
                _stepCycle += (_characterController.velocity.magnitude + (speed*(_isWalking ? 1f : _runstepLenghten)))*
                             Time.fixedDeltaTime;
            }

            if (!(_stepCycle > _nextStep))
            {
                return;
            }

            _nextStep = _stepCycle + _stepInterval;

            PlayFootStepAudio();
        }

        private void PlayFootStepAudio()
        {
            if (!_characterController.isGrounded)
            {
                return;
            }
            // pick & play a random footstep sound from the array,
            // excluding sound at index 0
            int n = Random.Range(1, _footstepSounds.Length);
            _audioSource.clip = _footstepSounds[n];
            _audioSource.PlayOneShot(_audioSource.clip);
            // move picked sound to index 0 so it's not picked next time
            _footstepSounds[n] = _footstepSounds[0];
            _footstepSounds[0] = _audioSource.clip;
        }

	    private void PlayLandingSound()
	    {
		    _audioSource.clip = _landSound;
		    _audioSource.Play();
		    _nextStep = _stepCycle + .5f;
	    }

		private void UpdateCameraPosition(float speed)
        {
            Vector3 newCameraPosition;
            if (_useHeadBob == false || _currentViewState == ViewState.ThirdPerson)
            {
                return;
            }
            if (_characterController.velocity.magnitude > 0 && _characterController.isGrounded)
            {
                _camera.transform.localPosition =
                    _headBob.DoHeadBob(_characterController.velocity.magnitude +
                                      (speed*(_isWalking ? 1f : _runstepLenghten)));
                newCameraPosition = _camera.transform.localPosition;
                newCameraPosition.y = _camera.transform.localPosition.y - _jumpBob.Offset();
            }
            else
            {
                newCameraPosition = _camera.transform.localPosition;
                newCameraPosition.y = _originalCameraPosition.y - _jumpBob.Offset();
            }
            _camera.transform.localPosition = newCameraPosition;
        }

        private void GetInput(out float speed)
        {
            // Read input
            float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
            float vertical = CrossPlatformInputManager.GetAxis("Vertical");

            bool waswalking = _isWalking;

#if MOBILE_INPUT == false
            // On standalone builds, walk/run speed is modified by a key press.
            // keep track of whether or not the character is walking or running
            _isWalking = !Input.GetKey(KeyCode.LeftShift);
#endif
            // set the desired speed to be walking or running
            speed = _isWalking ? _walkSpeed : _runSpeed;
            _input = new Vector2(horizontal, vertical);

            // normalize input if it exceeds 1 in combined length:
            if (_input.sqrMagnitude > 1)
            {
                _input.Normalize();
            }

            // handle speed change to give an fov kick
            // only if the player is going to a run, is running and the fovkick is to be used
            if (_isWalking != waswalking && _useFovKick && _characterController.velocity.sqrMagnitude > 0)
            {
                StopAllCoroutines();
                StartCoroutine(!_isWalking ? _fovKick.FOVKickUp() : _fovKick.FOVKickDown());
            }
        }

        private void RotateView()
        {
	        if (LockCamera)
		        return;

            _mouseLook.LookRotation(transform, _camera.transform, _currentViewState != ViewState.FirstPerson);
        }

        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            Rigidbody body = hit.collider.attachedRigidbody;
            //don't move the rigidbody if the character is on top of it
            if (_collisionFlags == CollisionFlags.Below)
            {
                return;
            }

            if (body == null || body.isKinematic)
            {
                return;
            }
            body.AddForceAtPosition(_characterController.velocity*0.1f, hit.point, ForceMode.Impulse);
        }
    }

	#endregion
}
