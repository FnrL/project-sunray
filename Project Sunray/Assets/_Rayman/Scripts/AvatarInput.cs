﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(Avatar))]
public class AvatarInput : MonoBehaviour
{

	#region Fields

	private Avatar _avatar;

	private bool _jump;

	private bool _helicopter;

	#endregion

	#region Constructor

	/// <summary> 
    /// Start is called before the first frame update
	/// </summary>
    private void Start()
	{
		_avatar = GetComponent<Avatar>();
	}

	#endregion

	#region Methods

	/// <summary> 
    /// Update is called once per frame
	/// </summary>
    private void Update()
    {
		//Register jump key pressed and cache until next FixedUpdate
	    if (_jump == false)
	    {
		    _jump = CrossPlatformInputManager.GetButtonDown("Jump");
	    }

		_helicopter = CrossPlatformInputManager.GetButton("Jump");

	    if (Input.GetKeyDown(KeyCode.G))
	    {
		    _avatar.ToggleGodMode();
	    }

	    if (CrossPlatformInputManager.GetButtonDown("Fire1"))
	    {
		    _avatar.ChargePunch();
	    }

		if (CrossPlatformInputManager.GetButtonUp("Fire1"))
	    {
		    _avatar.ReleasePunch();
	    }
	}

	private void FixedUpdate()
	{
		Vector2 input = new Vector2(CrossPlatformInputManager.GetAxis("Horizontal"),
									CrossPlatformInputManager.GetAxis("Vertical"));
		bool isCrouching = input.y < -0.1f;
		bool isSprinting = CrossPlatformInputManager.GetButton("Fire3");
		_avatar.Move(input, _jump, _helicopter, isCrouching, isSprinting);

		_jump = false;
	}

	#endregion

}
