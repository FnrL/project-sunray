﻿using UnityEngine;

public class FPSController : MonoBehaviour
{
	#region Types

	private enum ViewState
	{
		FirstPerson,
		ThirdPerson,
	}

	#endregion

	#region EditorFields

	/// <summary>
	/// Gun reference
	/// </summary>
	[SerializeField]
	private Gun _gun;

	/// <summary>
	/// FPS camera that is being used
	/// </summary>
	[Header("Look settings")]
	[SerializeField]
	private Camera _camera;

	[SerializeField]
	private Camera _gunCamera;

	/// <summary>
	/// Look sensitivity
	/// </summary>
	[SerializeField]
	private float _lookSensitivity;

	/// <summary>
	/// Field of view
	/// </summary>
	[SerializeField]
	private float _fov;

	/// <summary>
	/// Upper camera angle limit
	/// </summary>
	[SerializeField]
	[Range(-180,180)]
	private float _upperAngleLimit;

	/// <summary>
	/// Lower camera angle limit
	/// </summary>
	[SerializeField]
	[Range(-180, 180)]
	private float _lowerAngleLimit;

	/// <summary>
	/// Capsule collider of the player
	/// </summary>
	[Header("Move settings")]
	[SerializeField]
	private CapsuleCollider _collider;

	/// <summary>
	/// Rigidbody the fps controller is using
	/// </summary>
	[SerializeField]
	private Rigidbody _rigidbody;

	/// <summary>
	/// Movement speed
	/// </summary>
	[SerializeField]
	private float _speed;

	/// <summary>
	/// Sprint multiplier
	/// </summary>
	[SerializeField]
	private float _speedModifier = 1.5f;

	/// <summary>
	/// Jump force
	/// </summary>
	[SerializeField]
	private float _jumpForce = 1.0f;

	/// <summary>
	/// Transform to raycast from for jump check
	/// </summary>
	[SerializeField]
	private Transform _raycastTransform;

	/// <summary>
	/// Which layers to exclude when raycasting for jump check
	/// </summary>
	[SerializeField]
	private LayerMask _exLayerMask;

	[SerializeField]
	private Transform _firstPersonParent;

	[SerializeField]
	private Transform _thirdPersonParent;

	[SerializeField]
	private Transform _cameraParent;

	#endregion

	#region Propeties

	/// <summary>
	/// Players rigidbody
	/// </summary>
	public Rigidbody Rigidbody
	{
		get { return _rigidbody; }
	}

	/// <summary>
	/// The gun rendering camera
	/// </summary>
	public Camera GunCamera
	{
		get { return _gunCamera; }
	}

	/// <summary>
	/// FPS camera that is being used
	/// </summary>
	public Camera Camera
	{
		get { return _camera; }
	}

	#endregion

	#region Fields

	/// <summary>
	/// Is the speed boost modifier active
	/// </summary>
	private bool _modifierActive;

	private ViewState _currentViewState = ViewState.FirstPerson;

	#endregion

	/// <summary>
	/// Use this for initialization
	/// </summary>
	private void Start()
    {
		_camera.fieldOfView = _fov;
	    _gunCamera.fieldOfView = _fov;
		Cursor.lockState = CursorLockMode.Locked;
	    Cursor.visible = false;
	}

	/// <summary>
	/// Called every frame
	/// </summary>
	private void Update()
	{
		if (Input.GetKeyUp(KeyCode.F1))
		{
			ToggelViewState();
		}

		_modifierActive = Input.GetKey(KeyCode.LeftShift);
		if (Input.GetKeyDown(KeyCode.Space))
		{
			if (CanJump())
			{
				_rigidbody.AddForce(Vector3.up * _jumpForce, ForceMode.Impulse);
			}
		}


		if (Input.GetKey(KeyCode.Mouse0))
		{
			_gun.Fire();
		}

		if (Input.GetKeyDown(KeyCode.R))
		{
			_gun.Reload();
		}


		LookUpdate();
	}

	private void ToggelViewState()
	{
		switch (_currentViewState)
		{
			case ViewState.FirstPerson:
				_currentViewState = ViewState.ThirdPerson;
				Camera.transform.position = _thirdPersonParent.position;
				_cameraParent.transform.rotation = Quaternion.identity;
				break;
			case ViewState.ThirdPerson:
				_currentViewState = ViewState.FirstPerson;
				Camera.transform.position = _firstPersonParent.position;
				break;
		}
	}

	/// <summary>
	/// Called at a fixed time
	/// </summary>
    private void FixedUpdate()
	{
		Vector3 moveX, moveY, desiredPos = Vector3.zero;
	    switch (_currentViewState)
	    {
		    case ViewState.FirstPerson:
				moveX = transform.right * Input.GetAxisRaw("Horizontal");;
				moveY = transform.forward * Input.GetAxisRaw("Vertical");
			    desiredPos = (moveX + moveY).normalized * _speed * (_modifierActive ? _speedModifier : 1) * Time.fixedDeltaTime;

				break;
		    case ViewState.ThirdPerson:
			    moveX = transform.right * Input.GetAxisRaw("Horizontal"); ;
			    moveY = Camera.transform.forward * Input.GetAxisRaw("Vertical");
			    desiredPos = (moveX + moveY).normalized * _speed * (_modifierActive ? _speedModifier : 1) * Time.fixedDeltaTime;
				break;
	    }

		_rigidbody.MovePosition(transform.position + desiredPos);
	}


	/// <summary>
	/// Can we jump?
	/// </summary>
	/// <returns>can we jump?</returns>
	private bool CanJump()
	{
		RaycastHit hit;
		Ray ray = new Ray(transform.position, Vector3.down);
		if (Physics.Raycast(ray, out hit, 1.1f))
		{
			return true;
		}
		Debug.DrawRay(transform.position, Vector3.down , Color.red);

		return false;
	}

	private float _pitch;

	/// <summary>
	/// Update loop for FPS look logic
	/// </summary>
	private void LookUpdate()
	{
		_pitch += Input.GetAxisRaw("Mouse Y") * -_lookSensitivity * Time.deltaTime;
		_pitch = Mathf.Clamp(_pitch, _lowerAngleLimit, _upperAngleLimit);

		//Magic is done here
		switch (_currentViewState)
		{
			case ViewState.FirstPerson:
				_cameraParent.localRotation = Quaternion.Euler(Vector3.right * _pitch);
				transform.rotation *= Quaternion.Euler(Input.GetAxis("Mouse X") * _lookSensitivity * Time.deltaTime * Vector3.up);
				break;
			case ViewState.ThirdPerson:
				//_cameraParent.rotation = Quaternion.Euler(_pitch, _cameraParent.rotation.eulerAngles.y, _cameraParent.rotation.eulerAngles.z);
				_cameraParent.localRotation *= Quaternion.Euler(Input.GetAxis("Mouse X") * _lookSensitivity * Time.deltaTime * Vector3.up);
				break;
		}

	}
}
