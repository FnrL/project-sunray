﻿using UnityEngine;

public class JumpPad : MonoBehaviour
{

	#region EditorFields

	/// <summary>
	/// Direction to launch the player in
	/// </summary>
	[SerializeField]
	private Transform _jumpDirection;

	/// <summary>
	/// The power with which we are going to launch the player
	/// </summary>
	[SerializeField]
	private float _power;

	#endregion

	// Start is called before the first frame update
	private void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        
    }

	private void OnDrawGizmos()
	{
		if (_jumpDirection != null) 
		{
			Gizmos.color = Color.red;
			Gizmos.DrawLine(transform.position, _jumpDirection.position);
		}
	}

	private void OnTriggerEnter(Collider coll)
	{
		FPSController player = coll.GetComponent<FPSController>();
		if (player != null)
		{
			player.Rigidbody.AddForce((_jumpDirection.position - transform.position).normalized * _power, ForceMode.Impulse);
		}
	}
}
