﻿using UnityEngine;

public class CharacterObserver : MonoBehaviour
{
	#region Fields

	private Animator _animator;

	private Vector3 _prevPos;

	#endregion


    // Start is called before the first frame update
    private void Start()
    {
	    _animator = GetComponent<Animator>();
	    _prevPos = transform.position;

    }

    // Update is called once per frame
    private void Update()
    {
		 float speed = (_prevPos - transform.position).magnitude;
		_animator.SetFloat("Velocity", speed * 10);
		Debug.Log(speed * 10);
	    _prevPos = transform.position;
    }
}
