﻿using UnityEngine;

public class DisguisableObject : MonoBehaviour
{
	#region EditorFields

	[SerializeField]
	private GameObject _originalprefab;

	#endregion

	#region Properties

	public GameObject OrginalPrefab
	{
		get { return _originalprefab; }
	}

	#endregion

    // Start is called before the first frame update
    private void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        
    }
}
