﻿using UnityEngine;

public class Gun : MonoBehaviour , IGun
{
	#region EditorFields


	[SerializeField]
	private float _damage;

	/// <summary>
	/// Bullet prefab
	/// </summary>
	[SerializeField]
	private GameObject _bulletPrefab;

	/// <summary>
	/// Bullet spawn
	/// </summary>
	[SerializeField]
	private Transform _bulletSpawn;

	/// <summary>
	/// Bullet speed
	/// </summary>
	[SerializeField]
	private float _power;

	/// <summary>
	/// How many bullets does the gun shoot per second
	/// </summary>
	[SerializeField]
	private float _fireRate;

	[SerializeField]
	private bool _autoReload;

	/// <summary>
	/// Max count in magazine
	/// </summary>
	[SerializeField]
	private int _magazineSize = 60;

	/// <summary>
	/// Total amount of ammo in inventory
	/// </summary>
	[SerializeField]
	private int _totalAmmo = 1000;

	/// <summary>
	/// Ammo count in current magazine
	/// </summary>
	[SerializeField]
	private int _currentAmmo;

	#endregion

	#region Fields

	/// <summary>
	/// Time used for shooting
	/// </summary>
	private float _timer;
	

	#endregion

	#region Constructor

	// Start is called before the first frame update
	private void Start()
	{
		_currentAmmo = _magazineSize;
		SetLayerRecursively(gameObject,LayerMask.NameToLayer("Gun"));
	}

	#endregion

	#region Methods

	/// <summary>
	/// Recursively set GameObject and all childeren to specified layer
	/// </summary>
	/// <param name="obj">Gameobject to iterate over</param>
	/// <param name="layer">Layer name</param>
	private void SetLayerRecursively(GameObject obj, int layer)
	{
		if (null == obj)
			return;

		obj.layer = layer;
		foreach (Transform child in obj.transform)
		{
			if (null == child)
				continue;

			SetLayerRecursively(child.gameObject, layer);
		}
	}

	// Update is called once per frame
	private void Update()
    {
	    _timer += Time.deltaTime;
	}

	/// <summary>
	/// Reload the gun with ammo
	/// </summary>
	public void Reload()
	{
		Debug.Log("Add reload animation here");
		int shotBullets = _magazineSize - _currentAmmo;
		if (_totalAmmo - shotBullets > 0)
		{
			_currentAmmo = _magazineSize;
			_totalAmmo -= shotBullets;
		}
		else if (_totalAmmo > 0)
		{
			_currentAmmo = _totalAmmo;
			_totalAmmo -= shotBullets;
		}
		else if (_totalAmmo <= 0)
		{
			Debug.Log("Reload denied");
		}
	}

	/// <summary>
	/// Fire Gun
	/// </summary>
	public void Fire()
	{	
		if (_timer >= 1.0f / _fireRate)
		{
			if (_currentAmmo > 0)
			{
				_timer = 0.0f;

				//Hit scan
				Ray bullet = new Ray(_bulletSpawn.transform.position, _bulletSpawn.forward);
				RaycastHit hit;
				if (Physics.Raycast(bullet, out hit))
				{
					NetworkPlayer netPlayer = hit.collider.GetComponent<NetworkPlayer>();
					if (netPlayer != null)
					{
						//Player got hit localy
						netPlayer.HitLocal(_damage);
					}
				}

				//Projectile
				GameObject bulletGO = Instantiate(_bulletPrefab, _bulletSpawn.position, _bulletSpawn.rotation);
				bulletGO.GetComponent<Rigidbody>().AddForce(_bulletSpawn.transform.forward * _power, ForceMode.Impulse);
				_currentAmmo--;
			}
			else
			{
				if (_autoReload)
				{
					//Reload
					Reload();
				}
			}
		}
	}

	#endregion

}
