﻿using UnityEngine;

public class GizmoRender : MonoBehaviour
{
	#region Types

	/// <summary>
	/// Type of Gizmo
	/// </summary>
	public enum GizmoType
	{
		Sphere,
		Cube,
	}

	#endregion

	#region EditorFields

	/// <summary>
	/// If this is true then the gizmo will use localrotation instead of global
	/// </summary>
	[SerializeField]
	private bool _isLocalOrientation;

	/// <summary>
	/// Draw the origin's axis
	/// </summary>
	[SerializeField]
	private bool _drawOriginAxis = true;

	/// <summary>
	/// Gizmo shape we want
	/// </summary>
	[SerializeField]
	private GizmoType _gizmoType = GizmoType.Sphere;

	/// <summary>
	/// Gizmo size
	/// </summary>
	[SerializeField]
	private float _size = 0.5f;

	/// <summary>
	/// Should the wire frame be drawn?
	/// </summary>
	[SerializeField]
	private bool _drawWireFrame;

	/// <summary>
	/// Wire frame color
	/// </summary>
	[SerializeField]
	private Color _wireFrameColor = new Color(0, 0, 0, 1);

	/// <summary>
	/// Should the base shape be drawn?
	/// </summary>
	[SerializeField]
	private bool _drawBaseShape = true;

	/// <summary>
	/// Base shape color
	/// </summary>
	[SerializeField]
	private Color _baseColor = new Color(0.8f, 0.14f, 0.14f, 0.8f);


	#endregion

	#region Methods

	private void OnDrawGizmos()
	{
		//====================================
		// Legacy code
		//====================================
		//float radius = 0.5f;
		//Gizmos.color = new Color(1, 1, 1, 0.2f);
		//Gizmos.DrawSphere(transform.position, radius);
		//radius *= 1.1f;

		if (_isLocalOrientation)
		{
			Gizmos.matrix = transform.localToWorldMatrix;
		}

		//Base shape
		if (_drawBaseShape)
		{
			Gizmos.color = _baseColor;
			switch (_gizmoType)
			{
				case GizmoType.Sphere:
					Gizmos.DrawSphere(_isLocalOrientation ? Vector3.zero : transform.position, _size);
					break;
				case GizmoType.Cube:
					Gizmos.DrawCube(_isLocalOrientation ? Vector3.zero : transform.position, new Vector3(_size, _size, _size));
					break;
			}
		}

		//Wire Frame
		if (_drawWireFrame)
		{
			Gizmos.color = _wireFrameColor;
			switch (_gizmoType)
			{
				case GizmoType.Sphere:
					Gizmos.DrawWireSphere(_isLocalOrientation ? Vector3.zero : transform.position, _size);
					break;
				case GizmoType.Cube:
					Gizmos.DrawWireCube(_isLocalOrientation ? Vector3.zero : transform.position, new Vector3(_size, _size, _size));
					break;
			}
		}

		//Origin Axis
		if (_drawOriginAxis)
		{
			if (_isLocalOrientation)
			{
				Gizmos.color = Color.red;
				Gizmos.DrawLine(Vector3.zero, Vector3.right * _size);
				Gizmos.color = Color.green;
				Gizmos.DrawLine(Vector3.zero, Vector3.up * _size);
				Gizmos.color = Color.blue;
				Gizmos.DrawLine(Vector3.zero, Vector3.forward * _size);
			}
			else
			{
				Gizmos.color = Color.red;
				Gizmos.DrawLine(transform.position, transform.position + transform.right * _size);
				Gizmos.color = Color.green;
				Gizmos.DrawLine(transform.position, transform.position + transform.up * _size);
				Gizmos.color = Color.blue;
				Gizmos.DrawLine(transform.position, transform.position + transform.forward * _size);
			}
		}
	}

	#endregion
}
