﻿using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Hider : MonoBehaviour
{

	#region EditorFields

	/// <summary>
	/// Camera used
	/// </summary>
	[SerializeField]
	private Camera _camera;

	/// <summary>
	/// Which layers to exclude when raycasting for jump check
	/// </summary>
	[SerializeField]
	private LayerMask _exLayerMask;

	[SerializeField]
	private GameObject _disguiseParent;

	[SerializeField]
	private List<GameObject> _playerObjects;

	[SerializeField]
	private FirstPersonController _fpsController;


	#endregion

	#region Fields

	private DisguisableObject _currentDisguise;

	#endregion

	// Start is called before the first frame update
	private void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
	    if (Input.GetMouseButton(1))
	    {
		    DisguisableObject disgObj = CheckForValidDisguise();
		    if (disgObj != null)
		    {
				//Remove current disguise
			    if (_currentDisguise != null)
			    {
				    Destroy(_currentDisguise.gameObject);
			    }

			    SetPlayerObjectActive(false);
			    GameObject disguiseObject = Instantiate(Resources.Load("Prefabs/Disguises/" + disgObj.name, typeof(GameObject)) as GameObject, _disguiseParent.transform, false);
			    disguiseObject.transform.localPosition = Vector3.zero;
			    disguiseObject.layer = LayerMask.NameToLayer("Self");
			    _currentDisguise = disguiseObject.GetComponent<DisguisableObject>();

		    }
		}
		//Rotate Disguise
	    if (Input.GetKey(KeyCode.LeftControl))
	    {
		    if (_currentDisguise != null)
		    {
			    _fpsController.LockCamera = true;
				RotateDisguise();
		    }
	    }
	    else
	    {
			_fpsController.LockCamera = false;
		}

		//Clear disguise
	    if (Input.GetMouseButton(0))
	    {
		    ClearDisguise();
		}
    }

	private void RotateDisguise()
	{
		//TODO: Add sentisivity
		float yRot = Input.GetAxis("Mouse X");
		float xRot = Input.GetAxis("Mouse Y");

		_currentDisguise.transform.localRotation *= Quaternion.Euler(-xRot, yRot, 0f);

	}

	/// <summary>
	/// Clear this player of current disguise
	/// </summary>
	private void ClearDisguise()
	{
		if (_currentDisguise != null)
		{
			Destroy(_currentDisguise.gameObject);
		}
		SetPlayerObjectActive(true);
	}

	/// <summary>
	/// Check for valid disguise by ray casting the camera forward
	/// </summary>
	/// <returns>Found disguise</returns>
	private DisguisableObject CheckForValidDisguise()
	{
		RaycastHit hit;
		Ray ray = new Ray(_camera.transform.position, _camera.transform.forward);
		if (Physics.Raycast(ray, out hit, 10f, ~_exLayerMask))
		{
			Debug.Log(hit.transform.name);
			Debug.DrawRay(_camera.transform.position, _camera.transform.forward * 10, Color.red);
			return hit.transform.GetComponent<DisguisableObject>();
		}
		return null;
	}

	/// <summary>
	/// Iterate over all normal player character objects and enable or disable them
	/// </summary>
	/// <param name="active">To enable or disable</param>
	private void SetPlayerObjectActive(bool active)
	{
		for (int i = 0; i < _playerObjects.Count; i++)
		{
			_playerObjects[i].SetActive(active);
		}
	}
}
