﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
	#region EditorFields

	[SerializeField]
	private Transform _spawnPoint;

	#endregion

	#region Fields

	/// <summary>
	/// field of the singleton
	/// </summary>
	private static GameManager _instance;

	#endregion

	#region Properties

	/// <summary>
	/// Singleton
	/// </summary>
	public static GameManager Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = GameObject.FindObjectOfType<GameManager>();
			}

			return _instance;
		}
	}

	public Transform SpawnPoint
	{
		get { return _spawnPoint; }
	}

	public NetworkPlayer LocalPlayer { get; set; }

	#endregion

	#region Constructor

	private void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}

	/// <summary> 
    /// Start is called before the first frame update
	/// </summary>
    private void Start()
    {

    }

	#endregion

	#region Methods

	/// <summary> 
    /// Update is called once per frame
	/// </summary>
    private void Update()
    {
        
    }

	#endregion

}
