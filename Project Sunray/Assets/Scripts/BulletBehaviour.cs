﻿using System.Runtime.CompilerServices;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{

	#region EditorFields

	/// <summary>
	/// Time it take to destroy the bullet
	/// </summary>
	[SerializeField]
	private float _hitDestroyTime;

	/// <summary>
	/// The time it take to self destruct after not hitting anything
	/// </summary>
	[SerializeField]
	private float _selftDestroyTime = 10.0f;

	#endregion

	private void Start()
	{
		Destroy(gameObject, _selftDestroyTime);
	}
	
	private void OnTriggerEnter(Collider other)
	{
		Destroy(gameObject, _hitDestroyTime);
	} 

}
