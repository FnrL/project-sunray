﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class NetworkManager : Photon.Pun.MonoBehaviourPunCallbacks
{
	#region EditorFields

	[SerializeField]
	private Text _connectionText;

	[SerializeField]
	private Camera _lobbyCamera;

	[SerializeField]
	private GameObject _playerPrefab;

	[SerializeField]
	private bool _autoConnect;

	#endregion

	#region Fields

	private static NetworkManager _instance;

	#endregion

	#region Propterties

	public static NetworkManager Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = FindObjectOfType<NetworkManager>();
			}

			return _instance;
		}
	}

	#endregion

	#region Constructor

    // Start is called before the first frame update
	private void Start()
	{
		if (_autoConnect)
		{
			ConnectToPhoton();
		}
	}

	#endregion

	#region Methods

    // Update is called once per frame
    private void Update()
    {
	    _connectionText.text = PhotonNetwork.NetworkClientState.ToString();
    }

	/// <summary>
	/// Executed when connected to Photon master server
	/// </summary>
	public override void OnConnectedToMaster()
	{
		PhotonNetwork.JoinLobby();
	}

	/// <summary>
	/// Executed when joining a lobby
	/// </summary>
	public override void OnJoinedLobby()
	{
		PhotonNetwork.JoinOrCreateRoom("Room01", null, null);
		Debug.Log($"Joined Lobby: [{PhotonNetwork.CurrentLobby}]");
	}

	/// <summary>
	/// Executed when joining a room
	/// </summary>
	public override void OnJoinedRoom()
	{
		PhotonNetwork.Instantiate(_playerPrefab.name, GameManager.Instance.SpawnPoint.position,
			GameManager.Instance.SpawnPoint.rotation);
		Debug.Log($"Joined Room: [{PhotonNetwork.CurrentRoom.Name}][{PhotonNetwork.CurrentRoom.PlayerCount}]");
		_lobbyCamera.enabled = false;
	}

	/// <summary>
	/// Connect to photon using the defined settings
	/// </summary>
	public void ConnectToPhoton()
	{
		//Connect to Photon servers
		PhotonNetwork.ConnectUsingSettings();
	}

	#endregion


}
