﻿using Photon.Pun;
using UnityEngine;

public class NetworkPlayer : Photon.Pun.MonoBehaviourPun, IPunObservable
{

	#region EditorFields

	/// <summary>
	/// Fps controller
	/// </summary>
	[SerializeField]
	private FPSController _fpsController;

	/// <summary>
	/// Player controllers script to disable if they are not PhotonView.IsMine
	/// </summary>
	[SerializeField]
	private MonoBehaviour[] _playerControllers;

	/// <summary>
	/// players' health field
	/// </summary>
	[SerializeField]
	private float _health = 100.0f;

	#endregion

	#region Properties

	/// <summary>
	/// Players' Health property
	/// </summary>
	public float Health
	{
		get { return _health; }
	}

	public Camera Camera
	{
		get { return _fpsController.Camera; }
	}

	#endregion

	#region Constructor

	/// <summary> 
    /// Start is called before the first frame update
	/// </summary>
    private void Start()
	{
		Initialize();
	}

	private void Initialize()
	{
		if (photonView.IsMine)
		{
			GameManager.Instance.LocalPlayer = this;
		}
		else
		{
			_fpsController.Camera.enabled = false;
			_fpsController.GunCamera.enabled = false;

			foreach (MonoBehaviour m in _playerControllers)
			{
				m.enabled = false;
			}
		}
	}

	#endregion

	#region Methods

	void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.IsWriting)
		{
			stream.SendNext(_health);
		}
		else if (stream.IsReading)
		{
			_health = (float)stream.ReceiveNext();
		}
	}

	/// <summary>
	/// Player got hit local, we should let the whole server know this player has been damaged
	/// </summary>
	/// <param name="dmg">damage of the hit</param>
	public void HitLocal(float dmg)
	{
		photonView.RPC("TakeDamage", RpcTarget.All, dmg);
	}

	/// <summary>
	/// Tell everyone in the server that this networkPlayer got hit
	/// </summary>
	/// <param name="dmg">damage of the hit</param>
	[PunRPC]
	public void TakeDamage(float dmg)
	{
		_health -= dmg;

		if (_health <= 0)
		{
			Died();
		}
	}

	public void Died()
	{
		Debug.Log("Died");
	}

	#endregion

}
