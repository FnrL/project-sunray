﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuView : MonoBehaviour
{

	#region EditorFields

	/// <summary>
	/// Player button
	/// </summary>
	[SerializeField]
	private Button _playButton;

	/// <summary>
	/// Options button
	/// </summary>
	[SerializeField]
	private Button _optionsButton;

	/// <summary>
	/// Quit button
	/// </summary>
	[SerializeField]
	private Button _quitButton;

	#endregion

	#region Constructor

	/// <summary> 
    /// Start is called before the first frame update
	/// </summary>
    private void Start()
    {
        
    }

	#endregion

	#region Methods

	/// <summary> 
    /// Update is called once per frame
	/// </summary>
    private void Update()
    {
        
    }

	/// <summary>
	/// Play button is clicked
	/// </summary>
	public void PlayButtonClicked()
	{
		NetworkManager.Instance.ConnectToPhoton();
		gameObject.SetActive(false);
	}

	/// <summary>
	/// Options button is clicked
	/// </summary>
	public void OptionsButtonClicked()
	{
		//TODO: implement options
	}

	/// <summary>
	/// Quit button is clicked
	/// </summary>
	public void QuitButtonClicked()
	{
		Application.Quit();
	}

	#endregion

}
