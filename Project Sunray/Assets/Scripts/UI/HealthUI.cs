﻿using TMPro;
using UnityEngine;

public class HealthUI : MonoBehaviour
{

	#region EditorFields

	[SerializeField]
	private NetworkPlayer _player;

	#endregion

	#region Fields

	private TextMeshPro _healthIU;

	#endregion

    // Start is called before the first frame update
    void Start()
    {
	    _healthIU = GetComponent<TextMeshPro>();
    }

    // Update is called once per frame
    void Update()
    {
	    transform.rotation = Quaternion.LookRotation(GameManager.Instance.LocalPlayer.Camera.transform.forward, GameManager.Instance.LocalPlayer.Camera.transform.up);


		_healthIU.text = _player.Health.ToString();
	}
}
