﻿using UnityEngine;

public class Seeker : MonoBehaviour
{
	
	#region Constructor

	/// <summary> 
    /// Start is called before the first frame update
	/// </summary>
    private void Start()
    {
        
    }

	#endregion

	#region Editor Fields

	[SerializeField]
	private Gun _gun;

	#endregion

	#region Methods

	/// <summary> 
    /// Update is called once per frame
	/// </summary>
    private void Update()
    {
		if (Input.GetMouseButton(0))
		{
			_gun.Fire();
		}
    }

	#endregion

}
