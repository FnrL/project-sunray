﻿public interface IGun
{
	void Reload();
	void Fire();
}